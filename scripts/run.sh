#!/usr/bin/env bash

LOG=log.txt
RESULT=results.csv
TRAINING_FILE="/tmp/training-data.txt"

code=0
for test in `find tests -name "*.lb"`; do
   dlbatch "$test" | tee -a $LOG
   exit_code=${PIPESTATUS[0]}
   if [ "$exit_code" -ne 0 ]; then
      code=$exit_code
   fi
   if [[ $test == *"unittest/test_export"* ]]; then
      expected=$(dirname $test | sed 's/\//_/g')
      python scripts/check_export.py --file1 $TRAINING_FILE --file2 "data/unittest-data/validation/${expected}_training-data-expected.txt" || code=1
   fi
done

cat $LOG | grep "#RESULT#" | sed "s/#RESULT#/run1/g" >> $RESULT
echo "Benchmark report"
cat $RESULT

exit $code
