import sys
from optparse import OptionParser

if __name__ == "__main__":

   PARSER = OptionParser()                                                       
   PARSER.add_option('--file1', dest='file1', default='file1.txt', help='First trainning file')
   PARSER.add_option('--file2', dest='file2', default='file2.txt', help='Second trainning file')
                                                                                
   OPTIONS, ARGS = PARSER.parse_args()                                           
   file1 = OPTIONS.file1
   file2 = OPTIONS.file2

   features = {}
   with open(file1, "r") as f:
      for line in f:
         l = ",".join( sorted(line.strip("\n").split(",")) )
         features[l] = True

   print("features " + str(len(features)))
   with open(file2, "r") as f:
      for line in f:
         l = ",".join( sorted(line.strip("\n").split(",")) )
         if not l in features:
            print("has error, for example ")
            print(l)
            print(line)
            sys.exit(1)
