create test --overwrite

transaction
addBlock <doc>
  
  Feature(f),Feature_id(f:id) -> string(id).
  sku(f),sku_id(f:id) -> string(id).
  store(f) ,store_id(f:id) -> string(id).
  day(f),day_id(f:id) -> string(id).
  event(f),event_id(f:id) -> string(id).

  bias[] = val -> float(val).
  alpha[feat] = val -> Feature(feat), float(val).    
  betas[feat,k] = val -> Feature(feat), int(k), float(val).

  fcst_domain(sku, store, day) -> sku(sku), store(store), day(day).

  sku_has_feature[sku, feat] = val -> sku(sku), Feature(feat), float(val).
  store_has_feature[store, feat] = val -> store(store), Feature(feat), float(val).
  day_has_feature[day, feat] = val -> day(day), Feature(feat), float(val).

  sku_store_has_feature[sku, store, feat] = val -> sku(sku), store(store), Feature(feat), float(val).
  store_day_has_feature[store, day, feat] = val -> store(store), day(day), Feature(feat), float(val).
  sku_day_has_feature[sku, day, feat] = val -> sku(sku), day(day), Feature(feat), float(val).

  sku_store_day_has_feature[sku, store, day, feat] = val -> store(store), day(day), sku(sku), Feature(feat), float(val).
  
  sku_store_day_offer(sku, loc, day, event) -> sku(sku), store(loc), day(day), event(event).
  offer_has_feature[event, feat] = val -> event(event), Feature(feat), float(val).

  store_in_partition[store]=p -> store(store),int(p).

  fcst[sku, loc, day] = v -> sku(sku), store(loc), day(day), decimal(v).

  fcst_expected[sku, store, day] = v ->
  string(sku), string(store), string(day), decimal(v).

  fcst_round[sku, store, day] = v ->
  string(sku), string(store), string(day), decimal(v).

</doc>
commit


transaction
addBlock <doc>
   
  lang:compiler:disableError:AGG_DISJ[] = true.
  lang:compiler:disableWarning:AGG_DISJ[] = true.
    
  fcst[sku,store,day]=fcst <-  
   predict <<
      mode=eval,
      model=fmdirect,
      observation_keys=[sku|store|day],
      feature_key=feat,
      feature_value=featVal,
      coefficients=[bias=bias|alpha=alpha|beta=betas]
   >>
      (
         bias[]=_;
         alpha[feat]=_;
         betas[feat,_]=_
       ),
       fcst_domain(sku,store,day),
       (
         sku_store_day_offer(sku,store,day, offer),
         offer_has_feature[offer, feat] = featVal
         ;
         sku_store_has_feature[sku,store, feat]=featVal
         ;
         sku_store_day_has_feature[sku,store,day, feat]=featVal
         ;
         sku_day_has_feature[sku,day, feat]=featVal
         ;
         store_day_has_feature[store,day, feat]=featVal
         ;   
         sku_has_feature[sku, feat]=featVal
         ;
         store_has_feature[store, feat]=featVal
         ;
         day_has_feature[day, feat]=featVal
       ). 

</doc>
commit

transaction
exec <doc>

   /* empty coeff and empty feature predicates */

_in_fcstDomain(offset;sku,store,day) -> int(offset),string(sku),string(store),string(day).
   lang:physical:filePath[`_in_fcstDomain] = "data/unittest-data/fcst_domain.dlm".
   lang:physical:fileMode[`_in_fcstDomain] = "import".
   lang:physical:delimiter[`_in_fcstDomain] = "|".
   lang:physical:columnNames[`_in_fcstDomain] = "SKU,STORE,CAL".

  +store(st),+store_id[st]=store,
  +day(d),+day_id[d]=day,
  +sku(s),+sku_id[s]=sku,
  +fcst_domain(s,st,d) <- _in_fcstDomain(_;sku,store,day).

</doc>
commit

branchCreate thirtyone

transaction --branch thirtyone
exec <doc>
   _in_fcstExpected(offset;sku,store,day,v) -> 
   int(offset),string(sku),string(store),string(day),decimal(v).

   lang:physical:filePath[`_in_fcstExpected] = 
   "data/unittest-data/validation/test_fmdirect/fcst_expected_1.dlm".
   lang:physical:fileMode[`_in_fcstExpected] = "import".
   lang:physical:delimiter[`_in_fcstExpected] = "|".
   lang:physical:columnNames[`_in_fcstExpected] = "SKU,LOC,DAY,VALUE".

  +fcst_expected[sku,store,day]=v1 <- 
   _in_fcstExpected(_;sku,store,day,v),
   store_id[_]=store,
   day_id[_]=day, 
   sku_id[_]=sku,
   decimal:round2[v, 5] = v1.

  +fcst_round[sku, store, day] = v2 <-
    fcst[sk, st, d] = v1,
    store_id[st]=store,
    day_id[d]=day, 
    sku_id[sk]=sku,
    decimal:round2[v1, 5] = v2.

    fcst_round[sk,st,d]=val -> fcst_expected[sk, st, d]=val.
    fcst_expected[sk,st,d]=val -> fcst_round[sk, st, d]=val.

</doc>
commit

branchClose thirtyone

transaction
exec <doc>

   /* empty coefficients */

 _in_storeHasFeature(offset;store,f,v) -> int(offset), string(store),string(f), float(v).
   lang:physical:filePath[`_in_storeHasFeature] = "data/unittest-data/store_has_feature.dlm".
   lang:physical:fileMode[`_in_storeHasFeature] = "import".
   lang:physical:delimiter[`_in_storeHasFeature] = "|".
   lang:physical:columnNames[`_in_storeHasFeature] = "STORE,FEAT,VALUE".

  +Feature(feat),+Feature_id[feat]=f,
  +store_has_feature[st,feat]=v <- _in_storeHasFeature(_;store,f,v), store_id[st]=store.

 _in_skuHasFeature(offset;sku,f,v) -> int(offset), string(sku),string(f), float(v).
   lang:physical:filePath[`_in_skuHasFeature] = "data/unittest-data/sku_has_feature.dlm".
   lang:physical:fileMode[`_in_skuHasFeature] = "import".
   lang:physical:delimiter[`_in_skuHasFeature] = "|".
   lang:physical:columnNames[`_in_skuHasFeature] = "SKU,FEAT,VALUE".

  +Feature(feat),+Feature_id[feat]=f,
  +sku_has_feature[sk,feat]=v <- _in_skuHasFeature(_;sku,f,v), sku_id[sk]=sku.

 _in_dayHasFeature(offset;day,f,v) -> int(offset), string(day),string(f), float(v).
   lang:physical:filePath[`_in_dayHasFeature] = "data/unittest-data/cal_has_feature.dlm".
   lang:physical:fileMode[`_in_dayHasFeature] = "import".
   lang:physical:delimiter[`_in_dayHasFeature] = "|".
   lang:physical:columnNames[`_in_dayHasFeature] = "CAL,FEAT,VALUE".

  +Feature(feat),+Feature_id[feat]=f,
  +day_has_feature[d,feat]=v <- _in_dayHasFeature(_;day,f,v), day_id[d]=day.

 _in_skuDayHasFeature(offset;sku,day,f,v) -> int(offset),string(sku), string(day),string(f), float(v).
   lang:physical:filePath[`_in_skuDayHasFeature] = "data/unittest-data/sku_cal_has_feature.dlm".
   lang:physical:fileMode[`_in_skuDayHasFeature] = "import".
   lang:physical:delimiter[`_in_skuDayHasFeature] = "|".
   lang:physical:columnNames[`_in_skuDayHasFeature] = "SKU,CAL,FEAT,VALUE".

  +Feature(feat),+Feature_id[feat]=f,
  +sku_day_has_feature[sk,d,feat]=v <- _in_skuDayHasFeature(_;sku,day,f,v),
                                            sku_id[sk]=sku, day_id[d]=day.

 _in_skuStoreHasFeature(offset;sku,store,f,v) -> int(offset),string(sku), string(store),string(f), float(v).
   lang:physical:filePath[`_in_skuStoreHasFeature] = "data/unittest-data/sku_store_has_feature.dlm".
   lang:physical:fileMode[`_in_skuStoreHasFeature] = "import".
   lang:physical:delimiter[`_in_skuStoreHasFeature] = "|".
   lang:physical:columnNames[`_in_skuStoreHasFeature] = "SKU,STORE,FEAT,VALUE".

  +Feature(feat),+Feature_id[feat]=f,
  +sku_store_has_feature[sk,st,feat]=v <- _in_skuStoreHasFeature(_;sku,store,f,v),
                                           store_id[st]=store,sku_id[sk]=sku.

 _in_storeDayHasFeature(offset;store,day,f,v) -> int(offset),string(store), string(day),string(f), float(v).
   lang:physical:filePath[`_in_storeDayHasFeature] = "data/unittest-data/store_cal_has_feature.dlm".
   lang:physical:fileMode[`_in_storeDayHasFeature] = "import".
   lang:physical:delimiter[`_in_storeDayHasFeature] = "|".
   lang:physical:columnNames[`_in_storeDayHasFeature] = "STORE,CAL,FEAT,VALUE".

  +Feature(feat),+Feature_id[feat]=f,
  +store_day_has_feature[st,d,feat]=v <- _in_storeDayHasFeature(_;store,day,f,v),
                                           store_id[st]=store,day_id[d]=day.

 _in_skuStoreDayHasFeature(offset;sku,store,day,f,v) -> int(offset),string(sku),string(store), string(day),string(f), float(v).
   lang:physical:filePath[`_in_skuStoreDayHasFeature] = "data/unittest-data/sku_store_day_has_feature.dlm".
   lang:physical:fileMode[`_in_skuStoreDayHasFeature] = "import".
   lang:physical:delimiter[`_in_skuStoreDayHasFeature] = "|".
   lang:physical:columnNames[`_in_skuStoreDayHasFeature] = "SKU,STORE,DAY,FEAT,VALUE".

  +Feature(feat),+Feature_id[feat]=f,
  +sku_store_day_has_feature[sk,st,d,feat]=v <- _in_skuStoreDayHasFeature(_;sku,store,day,f,v), store_id[st]=store,day_id[d]=day, sku_id[sk]=sku.

 _in_offerHasFeature(offset;offer,f,v) -> int(offset),string(offer),string(f), float(v).
   lang:physical:filePath[`_in_offerHasFeature] = "data/unittest-data/offer_has_feature.dlm".
   lang:physical:fileMode[`_in_offerHasFeature] = "import".
   lang:physical:delimiter[`_in_offerHasFeature] = "|".
   lang:physical:columnNames[`_in_offerHasFeature] = "OFFER,FEAT,VALUE".

  +Feature(feat),+Feature_id[feat]=f,
  +event(offer),+event_id[offer]=evt,
  +offer_has_feature[offer,feat]=v <- _in_offerHasFeature(_;evt,f,v).

_in_skuLocDayOffer(offset;sku,store,day,offer) -> int(offset),string(sku),string(store),string(day),string(offer).
   lang:physical:filePath[`_in_skuLocDayOffer] = "data/unittest-data/sku_store_day_offer.dlm".
   lang:physical:fileMode[`_in_skuLocDayOffer] = "import".
   lang:physical:delimiter[`_in_skuLocDayOffer] = "|".
   lang:physical:columnNames[`_in_skuLocDayOffer] = "SKU,STORE,CAL,OFFER".

  +sku_store_day_offer(sk,st,d,evt) <- _in_skuLocDayOffer(_;sku,store,day,offer),
                                              store_id[st]=store,day_id[d]=day, sku_id[sk]=sku, event_id[evt]=offer.


</doc>
commit

branchCreate thirtytwo

transaction --branch thirtytwo
exec <doc>
   _in_fcstExpected(offset;sku,store,day,v) -> 
   int(offset),string(sku),string(store),string(day),decimal(v).

   lang:physical:filePath[`_in_fcstExpected] = 
   "data/unittest-data/validation/test_fmdirect/fcst_expected_2.dlm".
   lang:physical:fileMode[`_in_fcstExpected] = "import".
   lang:physical:delimiter[`_in_fcstExpected] = "|".
   lang:physical:columnNames[`_in_fcstExpected] = "SKU,LOC,DAY,VALUE".

  +fcst_expected[sku,store,day]=v1 <- 
   _in_fcstExpected(_;sku,store,day,v),
   store_id[_]=store,
   day_id[_]=day, 
   sku_id[_]=sku,
   decimal:round2[v, 5] = v1.

  +fcst_round[sku, store, day] = v2 <-
    fcst[sk, st, d] = v1,
    store_id[st]=store,
    day_id[d]=day, 
    sku_id[sk]=sku,
    decimal:round2[v1, 5] = v2.

    fcst_round[sk,st,d]=val -> fcst_expected[sk, st, d]=val.
    fcst_expected[sk,st,d]=val -> fcst_round[sk, st, d]=val.

</doc>
commit

branchClose thirtytwo


transaction
exec <doc>

   /* normal case, has everything */

 _in_bias(offset; p,c,v) -> int(p),int(offset), int(c), float(v).
   lang:physical:filePath[`_in_bias] = "data/unittest-data/bias.dlm".
   lang:physical:fileMode[`_in_bias] = "import".
   lang:physical:delimiter[`_in_bias] = "|".
   lang:physical:columnNames[`_in_bias] = "PARTITION,CLUSTER,VALUE".

  +bias[]=v <- 
  int:eq_2(p, 1),
  int:eq_2(c, 1),
  _in_bias(_;p,c,v).

 _in_betas(offset; p,c,f,index,v) ->
   int(p),int(offset), int(c),string(f), int(index),float(v).
   lang:physical:filePath[`_in_betas] = "data/unittest-data/beta.dlm".
   lang:physical:fileMode[`_in_betas] = "import".
   lang:physical:delimiter[`_in_betas] = "|".
   lang:physical:columnNames[`_in_betas] = "PARTITION,CLUSTER,FEAT,K,VALUE".

  +betas[ feat, index]=v <- 
  int:eq_2(p,1),
  int:eq_2(c,1),
  _in_betas(_;p,c,f,index,v), Feature_id[feat]=f.

 _in_alpha(offset; p,c,f,v) -> int(p),int(offset), int(c),string(f),float(v).
   lang:physical:filePath[`_in_alpha] = "data/unittest-data/alpha.dlm".
   lang:physical:fileMode[`_in_alpha] = "import".
   lang:physical:delimiter[`_in_alpha] = "|".
   lang:physical:columnNames[`_in_alpha] = "PARTITION,CLUSTER,FEAT,VALUE".

  +alpha[feat]=v <- 
  int:eq_2(p,1),
  int:eq_2(c,1),
  _in_alpha(_;p,c,f,v), Feature_id[feat]=f.

</doc>
commit


branchCreate thirtythree

transaction --branch thirtythree
exec <doc>
   _in_fcstExpected(offset;sku,store,day,v) -> 
   int(offset),string(sku),string(store),string(day),decimal(v).

   lang:physical:filePath[`_in_fcstExpected] = 
   "data/unittest-data/validation/test_fmdirect/fcst_expected_3.dlm".
   lang:physical:fileMode[`_in_fcstExpected] = "import".
   lang:physical:delimiter[`_in_fcstExpected] = "|".
   lang:physical:columnNames[`_in_fcstExpected] = "SKU,LOC,DAY,VALUE".

  +fcst_expected[sku,store,day]=v1 <- 
   _in_fcstExpected(_;sku,store,day,v),
   store_id[_]=store,
   day_id[_]=day, 
   sku_id[_]=sku,
   decimal:round2[v, 5] = v1.

  +fcst_round[sku, store, day] = v2 <-
    fcst[sk, st, d] = v1,
    store_id[st]=store,
    day_id[d]=day, 
    sku_id[sk]=sku,
    decimal:round2[v1, 5] = v2.

    fcst_round[sk,st,d]=val -> fcst_expected[sk, st, d]=val.
    fcst_expected[sk,st,d]=val -> fcst_round[sk, st, d]=val.

</doc>
commit

branchClose thirtythree

transaction
exec <doc>

   /* changes to coeffs predicates */

   ^alpha[feat] = 2f * val <-
    alpha@prev[feat] = val.

</doc>
commit

branchCreate thirtyfour

transaction --branch thirtyfour
exec <doc>
   _in_fcstExpected(offset;sku,store,day,v) -> 
   int(offset),string(sku),string(store),string(day),decimal(v).

   lang:physical:filePath[`_in_fcstExpected] = 
   "data/unittest-data/validation/test_fmdirect/fcst_expected_4.dlm".
   lang:physical:fileMode[`_in_fcstExpected] = "import".
   lang:physical:delimiter[`_in_fcstExpected] = "|".
   lang:physical:columnNames[`_in_fcstExpected] = "SKU,LOC,DAY,VALUE".

  +fcst_expected[sku,store,day]=v1 <- 
   _in_fcstExpected(_;sku,store,day,v),
   store_id[_]=store,
   day_id[_]=day, 
   sku_id[_]=sku,
   decimal:round2[v, 5] = v1.

  +fcst_round[sku, store, day] = v2 <-
    fcst[sk, st, d] = v1,
    store_id[st]=store,
    day_id[d]=day, 
    sku_id[sk]=sku,
    decimal:round2[v1, 5] = v2.

    fcst_round[sk,st,d]=val -> fcst_expected[sk, st, d]=val.
    fcst_expected[sk,st,d]=val -> fcst_round[sk, st, d]=val.

</doc>
commit

branchClose thirtyfour

transaction
exec <doc>

   /* all intersections are gone */

  -fcst_domain(s,st,d) <- 
  fcst_domain@prev(s, st, d).

</doc>
commit

branchCreate thirtyfive

transaction --branch thirtyfive
exec <doc>
   _in_fcstExpected(offset;sku,store,day,v) -> 
   int(offset),string(sku),string(store),string(day),decimal(v).

   lang:physical:filePath[`_in_fcstExpected] = 
   "data/unittest-data/validation/test_fmdirect/fcst_expected_5.dlm".
   lang:physical:fileMode[`_in_fcstExpected] = "import".
   lang:physical:delimiter[`_in_fcstExpected] = "|".
   lang:physical:columnNames[`_in_fcstExpected] = "SKU,LOC,DAY,VALUE".

  +fcst_expected[sku,store,day]=v1 <- 
   _in_fcstExpected(_;sku,store,day,v),
   store_id[_]=store,
   day_id[_]=day, 
   sku_id[_]=sku,
   decimal:round2[v, 5] = v1.

  +fcst_round[sku, store, day] = v2 <-
    fcst[sk, st, d] = v1,
    store_id[st]=store,
    day_id[d]=day, 
    sku_id[sk]=sku,
    decimal:round2[v1, 5] = v2.

    fcst_round[sk,st,d]=val -> fcst_expected[sk, st, d]=val.
    fcst_expected[sk,st,d]=val -> fcst_round[sk, st, d]=val.

</doc>
commit

branchClose thirtyfive

transaction
exec <doc>

  /* populate small forecast head */
  +fcst_domain(s,st,d) <- 
  store_id[st]=_,
  day_id[d]=_ ,
  sku_id[s]="10000020477".

  +fcst_domain(s,st,d) <- 
  store_id[st]="10000007715",
  day_id[d]=_ ,
  sku_id[s]="10000018708".

  +fcst_domain(s,st,d) <- 
  store_id[st]="10000007774",
  day_id[d]=_ ,
  sku_id[s]="10000018708".

  +fcst_domain(s,st,d) <- 
   store_id[st]="10000007761",
   day_id[d]="10000003811",
   sku_id[s]="10000018708".

   /* expect popcount 67081 */
</doc>
commit


branchCreate thirtysix

transaction --branch thirtysix
exec <doc>
   _in_fcstExpected(offset;sku,store,day,v) -> 
   int(offset),string(sku),string(store),string(day),decimal(v).

   lang:physical:filePath[`_in_fcstExpected] = 
   "data/unittest-data/validation/test_fmdirect/fcst_expected_6.dlm".
   lang:physical:fileMode[`_in_fcstExpected] = "import".
   lang:physical:delimiter[`_in_fcstExpected] = "|".
   lang:physical:columnNames[`_in_fcstExpected] = "SKU,LOC,DAY,VALUE".

  +fcst_expected[sku,store,day]=v1 <- 
   _in_fcstExpected(_;sku,store,day,v),
   store_id[_]=store,
   day_id[_]=day, 
   sku_id[_]=sku,
   decimal:round2[v, 5] = v1.

  +fcst_round[sku, store, day] = v2 <-
    fcst[sk, st, d] = v1,
    store_id[st]=store,
    day_id[d]=day, 
    sku_id[sk]=sku,
    decimal:round2[v1, 5] = v2.

    fcst_round[sk,st,d]=val -> fcst_expected[sk, st, d]=val.
    fcst_expected[sk,st,d]=val -> fcst_round[sk, st, d]=val.

</doc>
commit

branchClose thirtysix

transaction
exec <doc>

/*
 *    expect this query to be empty
 * 
 * data/unittest-data/data/unittest-data/bin/lb query testtwelve '_(sku, loc, day) 
 *    <- fcst_domain(sku, loc, day), 
 *       sku_id[sku]="10000018708",
 *       store_id[loc]="10000007748",
 *       day_id[day]="10000003811".'
 * 
 */

/*
 * remove all features for this intersection
 *    sku_id[sku]="10000018708", 
 *    store_id[loc]="10000007748", 
 *    day_id[day]="10000003811".'
 */

 -sku_store_day_offer(sku,store,day, offer) <-
  sku_store_day_offer@prev(sku,store,day, offer),
  store_id[store]="10000007748",
  day_id[day]="10000003811",
  sku_id[sku]="10000018708",
  event_id[offer]=_.

  
 -sku_store_has_feature[sku,store, feat]=_ <-
  sku_store_has_feature@prev[sku,store, feat]=_,
  store_id[store]="10000007748",
  sku_id[sku]="10000018708",
  Feature_id[feat]=_.

  -sku_store_day_has_feature[sku,store,day, feat]=_ <-
   sku_store_day_has_feature@prev[sku,store,day, feat] = _,
   store_id[store]="10000007748",
   day_id[day]="10000003811",
   sku_id[sku]="10000018708",
   Feature_id[feat]=_.

   -sku_day_has_feature[sku,day, feat]=_ <-
   sku_day_has_feature@prev[sku,day, feat]=_,
   day_id[day]="10000003811",
   sku_id[sku]="10000018708",
   Feature_id[feat]=_.

   -store_day_has_feature[store,day, feat]=_ <-
   store_day_has_feature@prev[store,day, feat]=_,
   store_id[store]="10000007748",
   day_id[day]="10000003811",
   Feature_id[feat]=_.

    -sku_has_feature[sku, feat]=_ <-
    sku_has_feature@prev[sku, feat]=_,
    sku_id[sku]="10000018708",
    Feature_id[feat]=_.

    -store_has_feature[store, feat]=_ <-
    store_has_feature@prev[store, feat]=_,
    store_id[store]="10000007748",
    Feature_id[feat]=_.

    -day_has_feature[day, feat]=_ <-
    day_has_feature@prev[day, feat]=_,
    day_id[day]="10000003811",
    Feature_id[feat]=_.

    /* expect popcount 67081 */

</doc>
commit


branchCreate thirtyseven

transaction --branch thirtyseven
exec <doc>
   _in_fcstExpected(offset;sku,store,day,v) -> 
   int(offset),string(sku),string(store),string(day),decimal(v).

   lang:physical:filePath[`_in_fcstExpected] = 
   "data/unittest-data/validation/test_fmdirect/fcst_expected_7.dlm".
   lang:physical:fileMode[`_in_fcstExpected] = "import".
   lang:physical:delimiter[`_in_fcstExpected] = "|".
   lang:physical:columnNames[`_in_fcstExpected] = "SKU,LOC,DAY,VALUE".

  +fcst_expected[sku,store,day]=v1 <- 
   _in_fcstExpected(_;sku,store,day,v),
   store_id[_]=store,
   day_id[_]=day, 
   sku_id[_]=sku,
   decimal:round2[v, 5] = v1.

  +fcst_round[sku, store, day] = v2 <-
    fcst[sk, st, d] = v1,
    store_id[st]=store,
    day_id[d]=day, 
    sku_id[sk]=sku,
    decimal:round2[v1, 5] = v2.

    fcst_round[sk,st,d]=val -> fcst_expected[sk, st, d]=val.
    fcst_expected[sk,st,d]=val -> fcst_round[sk, st, d]=val.

</doc>
commit

branchClose thirtyseven

transaction
exec <doc>

  /*
   * addition of this intersection should not affect
   * fcst since there are no features for this intersection
   */

  +fcst_domain(s,st,d) <- 
  store_id[st]="10000007748",
  day_id[d]="10000003811",
  sku_id[s]="10000018708".


/*
 *    expect this query to be empty
 * 
 *   data/unittest-data/data/unittest-data/bin/lb query testtwelve '_(sku, loc, day, fcst) <- 
 *    fcst[sku, loc, day]=fcst,
 *    sku_id[sku]="10000018708",
 *    store_id[loc]="10000007748",
 *    day_id[day]="10000003811".'
 * 
 *    expect popcount 105 
 */

</doc>
commit


branchCreate thirtyeight

transaction --branch thirtyeight
exec <doc>
   _in_fcstExpected(offset;sku,store,day,v) -> 
   int(offset),string(sku),string(store),string(day),decimal(v).

   lang:physical:filePath[`_in_fcstExpected] = 
   "data/unittest-data/validation/test_fmdirect/fcst_expected_8.dlm".
   lang:physical:fileMode[`_in_fcstExpected] = "import".
   lang:physical:delimiter[`_in_fcstExpected] = "|".
   lang:physical:columnNames[`_in_fcstExpected] = "SKU,LOC,DAY,VALUE".

  +fcst_expected[sku,store,day]=v1 <- 
   _in_fcstExpected(_;sku,store,day,v),
   store_id[_]=store,
   day_id[_]=day, 
   sku_id[_]=sku,
   decimal:round2[v, 5] = v1.

  +fcst_round[sku, store, day] = v2 <-
    fcst[sk, st, d] = v1,
    store_id[st]=store,
    day_id[d]=day, 
    sku_id[sk]=sku,
    decimal:round2[v1, 5] = v2.

    fcst_round[sk,st,d]=val -> fcst_expected[sk, st, d]=val.
    fcst_expected[sk,st,d]=val -> fcst_round[sk, st, d]=val.

</doc>
commit

branchClose thirtyeight

transaction
exec <doc>

/*
 * 
 *    addition of features for the sku key that would 
 *    allow the intersection:
 *     sku_id[sku]="10000018708",
 *     store_id[loc]="10000007748",
 *     day_id[day]="10000003811".'
 *    that is currently in the domain to appear in the fcst
 */

 _in_skuHasFeature(offset;sku,f,v) -> 
   int(offset), string(sku),string(f), float(v).
   lang:physical:filePath[`_in_skuHasFeature] = "data/unittest-data/sku_has_feature.dlm".
   lang:physical:fileMode[`_in_skuHasFeature] = "import".
   lang:physical:delimiter[`_in_skuHasFeature] = "|".
   lang:physical:columnNames[`_in_skuHasFeature] = "SKU,FEAT,VALUE".

  +sku_has_feature[sku,feat]=v <- 
   _in_skuHasFeature(_;sk,f,v), 
   Feature_id[feat]=f,
   sk="10000018708",
   sku_id[sku]=sk.

/*
 *    expect this result:
 * 
 *    data/unittest-data/data/unittest-data/bin/lb query testtwelve '_(sku, loc, day, fcst) <- 
 *       fcst[sku, loc, day]=fcst,
 *       sku_id[sku]="10000018708",
 *       store_id[loc]="10000007748",
 *       day_id[day]="10000003811".'
 * 
 *    /--------------- _ ---------------\
 *    [10000000005] "10000018708" 
 *    [10000000000] "10000007748" 
 *    [10000000025] "10000003811" 3.909487546160149041
 *    \--------------- _ ---------------/
 * 
 *    expect popcount 67082
 */

</doc>
commit 


branchCreate thirtynine

transaction --branch thirtynine
exec <doc>
   _in_fcstExpected(offset;sku,store,day,v) -> 
   int(offset),string(sku),string(store),string(day),decimal(v).

   lang:physical:filePath[`_in_fcstExpected] = 
   "data/unittest-data/validation/test_fmdirect/fcst_expected_9.dlm".
   lang:physical:fileMode[`_in_fcstExpected] = "import".
   lang:physical:delimiter[`_in_fcstExpected] = "|".
   lang:physical:columnNames[`_in_fcstExpected] = "SKU,LOC,DAY,VALUE".

  +fcst_expected[sku,store,day]=v1 <- 
   _in_fcstExpected(_;sku,store,day,v),
   store_id[_]=store,
   day_id[_]=day, 
   sku_id[_]=sku,
   decimal:round2[v, 5] = v1.

  +fcst_round[sku, store, day] = v2 <-
    fcst[sk, st, d] = v1,
    store_id[st]=store,
    day_id[d]=day, 
    sku_id[sk]=sku,
    decimal:round2[v1, 5] = v2.

    fcst_round[sk,st,d]=val -> fcst_expected[sk, st, d]=val.
    fcst_expected[sk,st,d]=val -> fcst_round[sk, st, d]=val.

</doc>
commit

branchClose thirtynine

transaction
exec <doc>

   /*
    * addition of feature should not affect the forecast
    * as this intersection is not in the domain
    */

  +sku(s),+sku_id[s]=sku,
  +sku_has_feature[s,feat]=1f <- 
   Feature_id[feat]=f,
   f="10000020845",
   sku = "555555555".

   /* expect popcount 67082 */

</doc>
commit 

branchCreate forty

transaction --branch forty
exec <doc>
   _in_fcstExpected(offset;sku,store,day,v) -> 
   int(offset),string(sku),string(store),string(day),decimal(v).

   lang:physical:filePath[`_in_fcstExpected] = 
   "data/unittest-data/validation/test_fmdirect/fcst_expected_10.dlm".
   lang:physical:fileMode[`_in_fcstExpected] = "import".
   lang:physical:delimiter[`_in_fcstExpected] = "|".
   lang:physical:columnNames[`_in_fcstExpected] = "SKU,LOC,DAY,VALUE".

  +fcst_expected[sku,store,day]=v1 <- 
   _in_fcstExpected(_;sku,store,day,v),
   store_id[_]=store,
   day_id[_]=day, 
   sku_id[_]=sku,
   decimal:round2[v, 5] = v1.

  +fcst_round[sku, store, day] = v2 <-
    fcst[sk, st, d] = v1,
    store_id[st]=store,
    day_id[d]=day, 
    sku_id[sk]=sku,
    decimal:round2[v1, 5] = v2.

    fcst_round[sk,st,d]=val -> fcst_expected[sk, st, d]=val.
    fcst_expected[sk,st,d]=val -> fcst_round[sk, st, d]=val.

</doc>
commit

branchClose forty

transaction
exec <doc>

  /*
   *  remove all features for intersection
   * store_id[st]="10000007748",
   * day_id[d]="10000003811",
   * sku_id[s]="10000018708".
   */

    -sku_has_feature[sku, feat]=_ <-
    sku_has_feature@prev[sku, feat]=_,
    Feature_id[feat]=_,
    sku_id[sku]="10000018708".

    /* expect popcount 67081 */

</doc>
commit

branchCreate fortyone

transaction --branch fortyone
exec <doc>
   _in_fcstExpected(offset;sku,store,day,v) -> 
   int(offset),string(sku),string(store),string(day),decimal(v).

   lang:physical:filePath[`_in_fcstExpected] = 
   "data/unittest-data/validation/test_fmdirect/fcst_expected_11.dlm".
   lang:physical:fileMode[`_in_fcstExpected] = "import".
   lang:physical:delimiter[`_in_fcstExpected] = "|".
   lang:physical:columnNames[`_in_fcstExpected] = "SKU,LOC,DAY,VALUE".

  +fcst_expected[sku,store,day]=v1 <- 
   _in_fcstExpected(_;sku,store,day,v),
   store_id[_]=store,
   day_id[_]=day, 
   sku_id[_]=sku,
   decimal:round2[v, 5] = v1.

  +fcst_round[sku, store, day] = v2 <-
    fcst[sk, st, d] = v1,
    store_id[st]=store,
    day_id[d]=day, 
    sku_id[sk]=sku,
    decimal:round2[v1, 5] = v2.

    fcst_round[sk,st,d]=val -> fcst_expected[sk, st, d]=val.
    fcst_expected[sk,st,d]=val -> fcst_round[sk, st, d]=val.

</doc>
commit

branchClose fortyone

transaction
exec <doc>
   
   /*
    * retraction from the domain s.t. there are no features
    * for this intersection
    */

  -fcst_domain(s,st,d) <- 
   fcst_domain@prev(s,st,d),
   store_id[st]="10000007748",
   day_id[d]="10000003811",
   sku_id[s]="10000018708".

    /* expect popcount 67081 */
</doc>
commit


branchCreate fortytwo

transaction --branch fortytwo
exec <doc>
   _in_fcstExpected(offset;sku,store,day,v) -> 
   int(offset),string(sku),string(store),string(day),decimal(v).

   lang:physical:filePath[`_in_fcstExpected] = 
   "data/unittest-data/validation/test_fmdirect/fcst_expected_12.dlm".
   lang:physical:fileMode[`_in_fcstExpected] = "import".
   lang:physical:delimiter[`_in_fcstExpected] = "|".
   lang:physical:columnNames[`_in_fcstExpected] = "SKU,LOC,DAY,VALUE".

  +fcst_expected[sku,store,day]=v1 <- 
   _in_fcstExpected(_;sku,store,day,v),
   store_id[_]=store,
   day_id[_]=day, 
   sku_id[_]=sku,
   decimal:round2[v, 5] = v1.

  +fcst_round[sku, store, day] = v2 <-
    fcst[sk, st, d] = v1,
    store_id[st]=store,
    day_id[d]=day, 
    sku_id[sk]=sku,
    decimal:round2[v1, 5] = v2.

    fcst_round[sk,st,d]=val -> fcst_expected[sk, st, d]=val.
    fcst_expected[sk,st,d]=val -> fcst_round[sk, st, d]=val.

</doc>
commit

branchClose fortytwo

transaction
exec <doc>

   /*
    * retraction from the feature predicate that should affect the
    * forecst
    */

   -sku_day_has_feature[sku,day, feat]=_ <-
   sku_day_has_feature@prev[sku,day, feat]=_,
   sku_id[sku]=_,
   Feature_id[feat] =_,
   day_id[day]="10000003810".

/*
 *    expect values in this query be different at 
 *    before and after this transaction
 * 
 *    data/unittest-data/data/unittest-data/bin/lb query testtwelve '_(day, fcst) <- 
 *       fcst[sku, loc, day]=fcst, 
 *       sku_id[sku]=_,
 *       store_id[loc]=_,
 *       day_id[day]="10000003810".'
 * 
 *    expect popcount 67081
 */

</doc>
commit


branchCreate fortythree

transaction --branch fortythree
exec <doc>
   _in_fcstExpected(offset;sku,store,day,v) -> 
   int(offset),string(sku),string(store),string(day),decimal(v).

   lang:physical:filePath[`_in_fcstExpected] = 
   "data/unittest-data/validation/test_fmdirect/fcst_expected_13.dlm".
   lang:physical:fileMode[`_in_fcstExpected] = "import".
   lang:physical:delimiter[`_in_fcstExpected] = "|".
   lang:physical:columnNames[`_in_fcstExpected] = "SKU,LOC,DAY,VALUE".

  +fcst_expected[sku,store,day]=v1 <- 
   _in_fcstExpected(_;sku,store,day,v),
   store_id[_]=store,
   day_id[_]=day, 
   sku_id[_]=sku,
   decimal:round2[v, 5] = v1.

  +fcst_round[sku, store, day] = v2 <-
    fcst[sk, st, d] = v1,
    store_id[st]=store,
    day_id[d]=day, 
    sku_id[sk]=sku,
    decimal:round2[v1, 5] = v2.

    fcst_round[sk,st,d]=val -> fcst_expected[sk, st, d]=val.
    fcst_expected[sk,st,d]=val -> fcst_round[sk, st, d]=val.

</doc>
commit

branchClose fortythree


transaction
exec <doc>
   
   /*
    * retraction from the domain s.t. there are features
    * for this intersection
    */

  -fcst_domain(s,st,d) <- 
   fcst_domain@prev(s,st,d),
   store_id[st]=_,
   day_id[d]="10000003810",
   sku_id[s]=_.

/*
 *    expect this query to be empty
 * 
 *    data/unittest-data/data/unittest-data/bin/lb query testtwelve '_(day, fcst) <- 
 *       fcst[sku, loc, day]=fcst, 
 *       sku_id[sku]=_,
 *       store_id[loc]=_,
 *       day_id[day]="10000003810".'
 * 
 *    expect popcount 65791
 */

</doc>
commit


branchCreate fortyfour

transaction --branch fortyfour
exec <doc>
   _in_fcstExpected(offset;sku,store,day,v) -> 
   int(offset),string(sku),string(store),string(day),decimal(v).

   lang:physical:filePath[`_in_fcstExpected] = 
   "data/unittest-data/validation/test_fmdirect/fcst_expected_14.dlm".
   lang:physical:fileMode[`_in_fcstExpected] = "import".
   lang:physical:delimiter[`_in_fcstExpected] = "|".
   lang:physical:columnNames[`_in_fcstExpected] = "SKU,LOC,DAY,VALUE".

  +fcst_expected[sku,store,day]=v1 <- 
   _in_fcstExpected(_;sku,store,day,v),
   store_id[_]=store,
   day_id[_]=day, 
   sku_id[_]=sku,
   decimal:round2[v, 5] = v1.

  +fcst_round[sku, store, day] = v2 <-
    fcst[sk, st, d] = v1,
    store_id[st]=store,
    day_id[d]=day, 
    sku_id[sk]=sku,
    decimal:round2[v1, 5] = v2.

    fcst_round[sk,st,d]=val -> fcst_expected[sk, st, d]=val.
    fcst_expected[sk,st,d]=val -> fcst_round[sk, st, d]=val.

</doc>
commit

branchClose fortyfour

transaction
exec <doc>

    ^store_has_feature[store, feat]=0.5f * val <-
     store_has_feature@prev[store, feat]= val,
     store_id[store] = "10000007774",
     Feature_id[feat] = _.

/*
 *      expect values for this query change before and after this
 *      transaction
 * 
 *       data/unittest-data/data/unittest-data/bin/lb query testtwelve '_(loc, fcst) <- 
 *          fcst[sku, loc, day]=fcst, 
 *          sku_id[sku]=_,
 *          store_id[loc]="10000007774",
 *          day_id[day]=_.'
 * 
 *       expect popcount 65791
 */

</doc>
commit


branchCreate fortyfive

transaction --branch fortyfive
exec <doc>
   _in_fcstExpected(offset;sku,store,day,v) -> 
   int(offset),string(sku),string(store),string(day),decimal(v).

   lang:physical:filePath[`_in_fcstExpected] = 
   "data/unittest-data/validation/test_fmdirect/fcst_expected_15.dlm".
   lang:physical:fileMode[`_in_fcstExpected] = "import".
   lang:physical:delimiter[`_in_fcstExpected] = "|".
   lang:physical:columnNames[`_in_fcstExpected] = "SKU,LOC,DAY,VALUE".

  +fcst_expected[sku,store,day]=v1 <- 
   _in_fcstExpected(_;sku,store,day,v),
   store_id[_]=store,
   day_id[_]=day, 
   sku_id[_]=sku,
   decimal:round2[v, 5] = v1.

  +fcst_round[sku, store, day] = v2 <-
    fcst[sk, st, d] = v1,
    store_id[st]=store,
    day_id[d]=day, 
    sku_id[sk]=sku,
    decimal:round2[v1, 5] = v2.

    fcst_round[sk,st,d]=val -> fcst_expected[sk, st, d]=val.
    fcst_expected[sk,st,d]=val -> fcst_round[sk, st, d]=val.

</doc>
commit

branchClose fortyfive



close --destroy
