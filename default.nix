{ cubiql_rewrite_optimizer_src ? ./.
, platform_release ? "4.4.4"
, nixpkgs ? <nixpkgs>
}:
with import <config/lib> { inherit nixpkgs; };
let
  logicblox = getLB platform_release;
  dev_data = import ./data.nix {inherit fetchs3; };
in 

  rec {
    build = buildLBConfig ({
        name = "pbs-apps";
        src = cubiql_rewrite_optimizer_src;
        buildInputs = [ logicblox ];
        propagatedBuildInputs = with pkgs; [ xvfb_run
                                             curl
                                             zip ];
        preConfigure = ''
          tar xzvf ${dev_data}
          ls data
        '';

        postInstall = ''
          ensureDir $out/nix-support
          ./scripts/run.sh
          cp results.csv $out/
          echo "file csv" $out/results.csv >> $out/nix-support/hydra-build-products
        '';

      }
    );

    binary_tarball = release_helper {
      inherit (build) name;
      inherit build;
    };

  }
