#Welcome!

This repo houses 

* the CubiQL Rewrite Optimizer; a LoqiQL library that contains helper predicates used to optimize some Measure Service generated CubiQL expressions.
* unit-tests and performance tests for P2P ML Forecasting rules.
* other tools developed by PBS team.

#Dev-data

To get dev-data to run tests for P2P forecasting rules, execute this script

* `./scripts/get-dev-data`

You might need to run `hologram me` to get appropriate credentials to access the dev-data.

#Build locally

Configure the build

* `lb config`

Build and install 

* `make`
* `make install`

Run tests for P2P ML Forecasting rules

* `make check`

#Hydra build

You can find all relevant Hydra builds for this project [here](https://bob.logicblox.com/project/cubiql-rewrite-optimizer)

#CubiQL Rewrite Optimizer Lib

For examples of how this library is used in other projects, check out:

* https://bitbucket.org/Predictix/crate_fm
* https://bitbucket.org/Predictix/walgreens_backend

Documentation about this library can be found [here](https://docs.google.com/document/d/10NWJ2DHwz_jWn5qXd3ngKPzh5XOvGW0_LkXmwHxIh7Q/edit)

#P2P ML Forecasting

P2P ML forecasting is part of an LB platform and its implementation can be found [here](https://bitbucket.org/logicblox/lb-database/src/be951ee0433fbec08fa5a58973aa7379b7ee1d53/BloxData/Datalob/blox/datalob/modules/ml/?at=default).

Documentation about how to use it can be found [here](https://docs.google.com/document/d/1_eNzJVo_c2bScufBy1mNVlKt7rBtOlj9SnZiXqj885M/edit)
