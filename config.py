from lbconfig.api import *

lbconfig_package(
  name='cubiqlRewriteOptimizer',
  version = '0.1',
  default_prefix = 'out'
)

depends_on(
  logicblox_dep, 
  lb_web_dep,
  measureBlox = { 
    'default_path' : '$(LB_MEASURE_SERVICE_HOME)',
    'help' : 'The BloxWeb Measure Service.'
  }
)

target('install', ['all'])

lb_library(
  name='cubiql_rewrite_optimizer',
  srcdir='src',
  deps={
    'lb_web' : '$(lb_web)',
    'lb_measure_service' : '$(measureBlox)/share/lb-measure-service'
  }
)

rule(
  output = 'install',
  commands = [
    'mkdir -p $(prefix)/ && cp -R  src $(prefix)/',
  ],
  input=[], 
  description='Install Cubiql Rewrite Optimizer'
)

# run tests
rule( output = 'check',                                                         
      input = [],                                                               
      commands = ['./scripts/run.sh'],                                      
      phony = True                                                              
)   

rule(
  output = 'clean',
  input = [],
  commands = [
   'rm -rf $(build)',
   'rm -rf $(prefix)'
  ],
  phony = True
)
